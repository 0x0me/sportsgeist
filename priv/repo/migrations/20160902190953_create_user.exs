defmodule Sportsgeist.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    # install citext extension for case insensitive texts
    execute("CREATE EXTENSION IF NOT EXISTS citext;")

    create table(:users) do
      add :email, :citext, null: false
      add :last_name, :string, null: true
      add :first_name, :string, null: true
      add :nickname, :string, size: 50, null: false
      add :phone, :string, null: true
      add :is_phone_call_allowed, :boolean, null: false, default: false

      add :is_enabled, :boolean, null: false, default: false
      add :is_admin, :boolean, null: false, default: false

      timestamps
    end

    create unique_index(:users, [:email])
    create unique_index(:users, [:nickname])
  end
end
