defmodule Sportsgeist.UserFromAuth do
  @moduledoc """
  Module is responsible for the actual user checking
  """
  alias Sportsgeist.User
  alias Sportsgeist.Authorization
  alias Ueberauth.Auth

  @min_length_password 8

  @doc """
  Load the user from the given information or create a new one if there
  is no user object and creation was requested.
  """
  def get_or_insert(auth, current_user, repo) do
    case auth_and_validate(auth, repo) do
      {:error, :not_found} -> register_user_from_auth(auth, current_user, repo)
      {:error, reason} -> {:error, reason}
      authorization ->
        if authorization.expires_at && authorization.expires_at < Guardian.Utils.timestamp do
          replace_authorization(authorization, auth, current_user, repo)
        else
          user_from_authorization(authorization, current_user, repo)
        end
    end
  end

  # authentificate and validate authorization for current user
  # for identity providers
  # validation is done by comparing the entered password from `auth.credentials.ohter.password`
  # with the stored on in `authorization.token`
  #
  # NOTE: having 'nil' as uid result from the auth will lead to an error when passed
  # to 'Repo.get_by/3'
  defp auth_and_validate(%{provider: :identity} = auth, repo) do
    case uid_from_auth(auth) do
      nil -> {:error, :no_user_given}
      uid ->
        case repo.get_by(Authorization, uid: uid, provider: to_string(auth.provider)) do
          nil -> {:error, :not_found}
          authorization ->
            # match passwords
            # stored known password is in `token`
            # entered password is in `auth.credentials.other.password`
            other_password = auth.credentials.other.password
            case other_password do
              pass when is_binary(pass) -> compare_passwords(other_password, authorization)
              _ -> {:error, :password_required}
            end
        end
    end
  end


  # compare password with token
  # returns `authorization`if passwords matching, otherwise {:error, :password_does_not_match}
  defp compare_passwords(password, authorization) do
    if Comeonin.Bcrypt.checkpw(password, authorization.token) do
      authorization
    else
      {:error, :password_does_not_match}
    end
  end

  # register user
  # create a user if all requirements for user creation are fulfilled
  defp register_user_from_auth(auth, current_user, repo) do
    case validate_auth_for_registration(auth) do
      {:error, reason} -> {:error, reason}
      :ok ->
        tx = repo.transaction(fn -> create_user_from_auth(auth, current_user, repo) end)
        case tx do
          {:ok, response} -> response
          {:error, reason} -> {:error, reason}
        end
    end
  end

  # user validation if a new user should be created
  # identity provider
  defp validate_auth_for_registration(%Auth{provider: :identity} = auth) do
    pw = Map.get(auth.credentials.other, :password)
    pwc = Map.get(auth.credentials.other, :password_confirmation)
    email = auth.info.email
    # password should be neither null nor empty and password and password
    # confirmation should be the same
    case pw do
      nil -> {:error, :password_is_null}
      ""  -> {:error, :password_empty}
      ^pwc -> validate_pw_length(pw, email)
      _ -> {:error, :password_confirmation_does_not_match}
    end
  end

  # all other providers should be fine?
  defp validate_auth_for_registration(_auth), do: :ok

  # validate password and delegate to email validation
  defp validate_pw_length(pw, email) when is_binary(pw) do
    if String.length(pw) >= @min_length_password do
      validate_email(email)
    else
      {:error, :password_to_short}
    end
  end

  # validiate email
  defp validate_email(email) when is_binary(email) do
    case Regex.run(~r/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/, email) do
      nil ->
        {:error, :invalid_email}
      [_email] ->
        :ok
    end
  end

  # create user in database
  defp create_user_from_auth(auth, current_user, repo) do
    user = current_user
    user = if !user, do: repo.get_by(User, email: auth.info.email)
    user = if !user, do: create_user(auth, repo)
    authorization_from_auth(user, auth, repo)
    {:ok, user}
  end

  # The values are taken from the Ueberauth.Auth object. Therefore there are
  # already defined properties for providing certain information (eg. nickname)-
  # Those information has to be looked up on known places (eg. 'auth.info'), whereas
  # additional information from the request is located at 'auth.extra'
  defp create_user(auth, repo) do
    new_user_values = %{
      email: auth.info.email,
      nickname: nickname_from_auth(auth),
      last_name: auth.info.last_name,
      first_name: auth.info.first_name,
      phone: auth.info.phone,
      # NOTE: need to implement Chars protocol
      #is_phone_call_allowed: is_phone_call_allowed_from_auth(auth)
    }

    result = User.changeset(%User{}, scrub(new_user_values))
    |> repo.insert

    case result do
      {:ok, user} -> user
      {:error, reason} -> repo.rollback(reason)
    end
  end

  # replace authorization with a new one
  defp replace_authorization(authorization, auth, current_user, repo) do
    case validate_auth_for_registration(auth) do
      {:error, reason} -> {:error, reason}
      :ok ->
        case user_from_authorization(authorization, current_user, repo) do
          {:error, reason} -> {:error, reason}
          {:ok, user} ->
            case repo.transaction(fn ->
              repo.delete(authorization)
              authorization_from_auth(user, auth, repo)
              user
            end) do
              {:ok, user} -> {:ok, user}
              {:error, reason} -> {:error, reason}
            end
        end
    end
  end

  # load user based on authorization
  defp user_from_authorization(authorization, current_user, repo) do
    case repo.one(Ecto.assoc(authorization, :user)) do
      nil -> {:error, :user_not_found}
      user ->
        if current_user && current_user.id != user.id do
          {:error, :user_does_not_match}
        else
          {:ok, user}
        end
    end
  end

  # create authorization
  defp authorization_from_auth(user, auth, repo) do
    authorization = %{
      provider: to_string(auth.provider),
      uid: uid_from_auth(auth),
      token: token_from_auth(auth),
      refresh_token: auth.credentials.refresh_token,
      expires_at: auth.credentials.expires_at,
      password: password_from_auth(auth),
      password_confirmation: password_confirmation_from_auth(auth),
      is_phone_call_allowed:  is_phone_call_allowed_from_auth(auth)
    }

    result = user
    |> Ecto.build_assoc(:authorizations)
    |> Authorization.changeset(scrub(authorization))
    |> repo.insert

    case result do
      {:ok, the_auth} -> the_auth
      {:error, reason} -> repo.rollback(reason)
    end
  end

  # get uid from authorization
  defp uid_from_auth(auth), do: auth.uid

  defp nickname_from_auth(auth), do: auth.info.nickname

  defp is_phone_call_allowed_from_auth(auth), do: Map.get(auth.extra.raw_info, "is_phone_call_allowed")

  # token from auth for identiy provider
  defp token_from_auth(%{provider: :identity} = auth) do
    case auth do
      %{credentials: %{other: %{password: pass}}} when not is_nil(pass) ->
        Comeonin.Bcrypt.hashpwsalt(pass)
      _ -> nil
    end
  end

  defp password_from_auth(%{provider: :identity} = auth), do: auth.credentials.other.password

  defp password_confirmation_from_auth(%{provider: :identity} = auth), do: auth.credentials.other.password_confirmation

  # We don't have any nested structures in our params that we are using scrub with so this is a very simple scrub
  defp scrub(params) do
    result = params
    |> Enum.filter(fn
      {_key, val} when is_binary(val) -> String.strip(val) != ""
      {_key, val} when is_nil(val) -> false
      _ -> true
    end)
    |> Enum.into(%{})
    result
  end
end
