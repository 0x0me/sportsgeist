defmodule Sportsgeist.AuthControllerTest do
  use Sportsgeist.ConnCase

  import Sportsgeist.Factory

  alias Sportsgeist.Repo
  alias Sportsgeist.GuardianToken

  setup do
    user_auth = insert(:user) |> with_authorization

    {:ok, %{
        user: user_auth.user,
        user_auth: user_auth,
      }
    }
  end

  test "cannot logout non-logged-in user", _context do
    conn = build_conn()
    |> get("/")

    {:error, :no_session} = Guardian.Plug.claims(conn)


    assert Guardian.Plug.current_resource(conn) == nil

    conn = delete recycle(conn), "/logout"

    assert get_flash(conn, :info) == "Not logged in"
  end

  test "DELETE /logout logs out the user", context do
    conn = guardian_login(context.user, :token)
      #|> guardian_login(context.admin, :token, key: :admin)
      |> get("/") # This get loads the info out of the session and puts it into the connection

    # did not work know
    assert Guardian.Plug.current_resource(conn).id == context.user.id

    {:ok, user_claims} = Guardian.Plug.claims(conn)
    user_jti = Map.get(user_claims, "jti")
    refute Repo.get_by!(GuardianToken, jti: user_jti) == nil

    # now lets logout from the main logout and make sure they're both clear
    conn = delete recycle(conn), "/logout"

    assert get_flash(conn, :info) === "Signed out"

    assert Guardian.Plug.current_resource(conn) == nil

    assert Repo.get_by(GuardianToken, jti: user_jti) == nil
  end

  test "Cannot login using the wrong password", context do
    conn =  build_conn()
    |> post("/auth/identity/callback", [email: context.user.email, password: "wrong password"])

    assert Guardian.Plug.current_resource(conn) == nil
    assert get_flash(conn, :error) == "Could not authenticate. Error: password_does_not_match"
  end

  test "Cannot login unkown user", _context do
    conn =  build_conn()
    |> post("/auth/identity/callback", [email: "unknown@example.org", password: "wrong password"])

    assert Guardian.Plug.current_resource(conn) == nil
    assert get_flash(conn, :error) == "Could not authenticate. Error: password_confirmation_does_not_match"
  end

  test "Login using password", context do
    conn =  build_conn()
    |> post("/auth/identity/callback", [email: context.user.email, password: "sekrit"])

    assert Guardian.Plug.current_resource(conn).id == context.user.id
    assert get_flash(conn, :info) == "Signed in as #{context.user.nickname}"

    {:ok, user_claims} = Guardian.Plug.claims(conn)
    user_jti = Map.get(user_claims, "jti")
    refute Repo.get_by!(GuardianToken, jti: user_jti) == nil
  end
end
