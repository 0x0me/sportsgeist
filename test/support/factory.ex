defmodule Sportsgeist.Factory do
  @moduledoc """
  Generators for test da using ExMachina
  """
  use ExMachina.Ecto, repo: Sportsgeist.Repo

  alias Sportsgeist.User
  alias Sportsgeist.Authorization
  alias Sportsgeist.GuardianToken

  def user_factory() do
    %User{
      nickname: sequence(:nickname, &"P-#{&1}"),
      email: sequence(:email, &"email-#{&1}@example.com"),
      last_name: "Doe",
      first_name: "John",
      phone: "12345-010"
    }
  end

  def factory(:guardian_token) do
    %GuardianToken{
      jti: sequence(:jti, &"jti-#{&1}"),
    }
  end

  def authorization_factory() do
    %Authorization{
      uid: sequence(:uid, &"uid-#{&1}"),
      user: build(:user),
      provider: "identity",
      token: Comeonin.Bcrypt.hashpwsalt("sekrit")
    }
  end

  def with_authorization(user, opts \\ []) do
    opts = opts ++ [user: user, uid: user.email]
    insert(:authorization, opts)
  end
end
