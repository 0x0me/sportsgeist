defmodule Sportsgeist.AuthController do
  @moduledoc """
  #Authentification controller
  Ueberauth integration.
  """
  use Sportsgeist.Web, :controller

  alias Sportsgeist.UserFromAuth

  plug Ueberauth

  @doc """
  Provide login to user
  """
  def login(conn, _params, current_user, _claims) do
    render conn, "login.html", current_user: current_user, current_auths: auths(current_user)
  end

  @doc """
  Handle authentification failures by displaying a flash message and redirecting
  to the login page
  """
  def callback(%Plug.Conn{assigns: %{ueberauth_failure: fails}} = conn, _params, current_user, _claims) do
    conn
    |> put_flash(:error, hd(fails.errors).message)
    |> render("login.html", current_user: current_user, current_auths: auths(current_user))
  end

  @doc """
  Handle authentification results by either granting access or registering a new
  user.
  This function delegates the validation to the `UserFromAuth` helper
  """
  def callback(%Plug.Conn{assigns: %{ueberauth_auth: auth}} = conn, _params, current_user, _claims) do
    case UserFromAuth.get_or_insert(auth, current_user, Repo) do
      {:ok, user} ->
        conn
        |> put_flash(:info, "Signed in as #{user.nickname}")
        |> Guardian.Plug.sign_in(user, :access, perms: %{default: Guardian.Permissions.max})
        |> redirect(to: "/")
      {:error, reason} ->
        conn
        |> put_flash(:error, "Could not authenticate. Error: #{reason}")
        |> render("login.html", current_user: current_user, current_auths: auths(current_user))
    end
  end

  @doc """
  Logout. Clear the whole session
  """
  def logout(conn, _params, current_user, _claims) do
    if current_user do
      conn
      |> Guardian.Plug.sign_out
      |> put_flash(:info, "Signed out")
      |> redirect(to: "/")
    else
      conn
      |> put_flash(:info, "Not logged in")
      |> redirect(to: "/")
    end
  end



  # load providers for user,
  # return empty list if no user given
  defp auths(nil), do: []

  # otherwise try to load it from the database
  defp auths(%Sportsgeist.User{} = user) do
    #TODO: Replace with Ecto.assoc/2 ?
    user
    |> Ecto.assoc(:authorizations)
    |> Repo.all
    |> Enum.map(&(&1.provider))
  end
end
