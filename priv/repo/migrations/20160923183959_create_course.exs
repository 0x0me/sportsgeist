defmodule Sportsgeist.Repo.Migrations.CreateCourse do
  @moduledoc """
  # Course
  A course is a certain, reoccuring training activity at a specific place and
  time. It is a kind of template for creating training events aka sessions. Courses
  are owned by at least one personen (trainer, owner, administrator) which could
  edit or delete the course as well as add other "administrative" persons.
  """
  use Ecto.Migration

  def change do
    create table(:courses) do
      add :name, :string, size: 150, null: false
      add :iteration, :string
      add :description, :text, null: true
      add :icon, :binary , null: true
      add :active, :boolean, null: false, default: true

      timestamps
    end

    create unique_index(:courses, [:name])

    create table(:owners, primary_key: false) do
      add :course_id, references(:courses)
      add :user_id, references(:users)

      timestamps
    end

    alter table(:owners) do
      modify :inserted_at, :datetime, default: fragment("NOW()")
      modify :updated_at, :datetime, default: fragment("NOW()")
    end
  end
end
