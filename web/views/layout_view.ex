defmodule Sportsgeist.LayoutView do
  use Sportsgeist.Web, :view

  @doc """
  Returns "active" if the currently request path is equal to the path definition
  expected (from the menu entry) given by path
  """
  def is_active(conn, expected_path) do
    if Regex.regex?(expected_path) do
      if String.match?(conn.request_path,expected_path) do
        "active"
      else
        ""
      end
    else
      if conn.request_path == expected_path do
        "active"
      else
        ""
      end
    end
  end

  def course_image() do
    :rand.seed(:exsplus)
    ["pexels-photo-60230.jpeg","pexels-photo-87835.jpeg","sport-fitness-workout-resolution.jpg"]
    |> Enum.random
  end
end
