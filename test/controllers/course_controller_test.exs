defmodule Sportsgeist.CourseControllerTest do
  use Sportsgeist.ConnCase

  import Sportsgeist.Factory

  alias Sportsgeist.Repo
  alias Sportsgeist.GuardianToken
  alias Sportsgeist.Course

  setup do
    Gettext.put_locale(Sportsgeist.Gettext, "en_US")

    user_auth = :user
    |> insert
    |> with_authorization

    {:ok, %{
        user: user_auth.user,
        user_auth: user_auth,
      }
    }
  end



  test "create a new course", context do
    conn = build_conn()
    |> login_as(context.user, "sekrit")

    other_owner = :user
    |> insert

    name = "Test Course Name"

    params = %{
      "description" => "desc",
      "iteration" => "it",
      "name" => name,
      "owner" => %{
        "0" => %{
          "id" => context.user.id,
          "nickname" => context.user.nickname
        },
        "1" => %{
          "id" => other_owner.id,
          "nickname" => other_owner.nickname
        }
      }
    }

    conn = conn
    |> post(course_path(conn, :create), %{"course" => params})

    assert get_flash(conn, :info) === "#{name} created!"
    refute get_flash(conn, :error)

    result = Repo.all(from c in Course, where: c.name == ^name, select: c)
    |> Repo.preload(:owner)

    assert Enum.count(result) === 1

    course = result
    |> List.first

    assert Enum.count(course.owner) === 2
  end

  test "it is not allowed to create a course without an owner", context do
    conn = build_conn()
    |> login_as(context.user, "sekrit")

    name = "Test Course Name"

    params = %{
      "description" => "desc",
      "iteration" => "it",
      "name" => name
    }

    conn = conn
    |> post(course_path(conn, :create), %{"course" => params})

    errors = conn.assigns.changeset.errors
    assert errors !== nil

    assert errors === [owner: {"can't be blank", [validation: :required]}]

    assert Repo.all(Course) === []

  end

  test "update an existing course", context do
    conn = build_conn()
    |> login_as(context.user, "sekrit")

    other_owner = :user
    |> insert

    name = "Test Course Name"

    course = %Course{
      name: name,
      description: "This is a very descriptive description.",
      owner: [context.user, other_owner],
      iteration: "nightly unless the moon is shining"
    }

    insert course

    result = Repo.all(from c in Course, where: c.name == ^name, select: c)
    |> Repo.preload(:owner)


    assert Enum.count(result) === 1
    course = result |> List.first

    params = %{
      "id" => course.id,
      "name" => "updated name"
    }

    conn = conn
    |> post(course_path(conn, :update, course.id), %{"course" => params, "_method" => "put"})



    assert get_flash(conn, :info) === "Course sucessfully updated."
    refute get_flash(conn, :error)


    result = Repo.one(from c in Course, where: c.id == ^course.id, select: c)
    |> Repo.preload(:owner)

    assert result.name === "updated name"
  end

  test "fail to update an existing course if the name is empty", context do
    conn = build_conn()
    |> login_as(context.user, "sekrit")

    other_owner = :user
    |> insert

    name = "Test Course Name"

    course = %Course{
      name: name,
      description: "This is a very descriptive description.",
      owner: [context.user, other_owner],
      iteration: "nightly unless the moon is shining"
    }

    insert course

    result = Repo.all(from c in Course, where: c.name == ^name, select: c)
    |> Repo.preload(:owner)


    assert Enum.count(result) === 1
    course = result |> List.first

    params = %{
      "id" => course.id,
      "name" => ""
    }

    conn = conn
    |> post(course_path(conn, :update, course.id), %{"course" => params, "_method" => "put"})



    errors = conn.assigns.changeset.errors
    assert errors !== nil

    assert errors === [name: {"can't be blank", [validation: :required]}]

    result = Repo.one(from c in Course, where: c.id == ^course.id, select: c)
    |> Repo.preload(:owner)

    assert result.name === name
  end


  defp login_as(connection, user, password) do
    conn = connection
    |> post("/auth/identity/callback", [email: user.email, password: password])

    assert Guardian.Plug.current_resource(conn).id == user.id
    assert get_flash(conn, :info) == "Signed in as #{user.nickname}"

    {:ok, user_claims} = Guardian.Plug.claims(conn)
    user_jti = Map.get(user_claims, "jti")
    refute Repo.get_by!(GuardianToken, jti: user_jti) == nil
    conn
  end
end
