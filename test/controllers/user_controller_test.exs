defmodule Sportsgeist.UserControllerTest do
  use Sportsgeist.ConnCase


  import Sportsgeist.Factory

  alias Sportsgeist.Repo
  alias Sportsgeist.GuardianToken

  alias Sportsgeist.User
  alias Sportsgeist.Authorization


  setup do
    Gettext.put_locale(Sportsgeist.Gettext, "en_US")

    user_auth = :user
    |> insert
    |> with_authorization

    {:ok, %{
        user: user_auth.user,
        user_auth: user_auth,
      }
    }
  end

  test "cannot change password if user id does not match expected user form login", context do
    conn =  build_conn()
    |> post("/auth/identity/callback", [email: context.user.email, password: "sekrit"])

    assert Guardian.Plug.current_resource(conn).id == context.user.id
    assert get_flash(conn, :info) == "Signed in as #{context.user.nickname}"

    {:ok, user_claims} = Guardian.Plug.claims(conn)
    user_jti = Map.get(user_claims, "jti")
    refute Repo.get_by!(GuardianToken, jti: user_jti) == nil

    conn = conn
    |> post(user_path(conn, :change_password, -1), [old_password: "abc", password: "wrong password", password_confirmation: "x"])

    assert get_flash(conn, :error) === "Unexpected user id."
  end

  test "cannot change profile information if user id does not match expected user form login", context do
    conn =  build_conn()
    |> post("/auth/identity/callback", [email: context.user.email, password: "sekrit"])

    assert Guardian.Plug.current_resource(conn).id == context.user.id
    assert get_flash(conn, :info) == "Signed in as #{context.user.nickname}"

    {:ok, user_claims} = Guardian.Plug.claims(conn)
    user_jti = Map.get(user_claims, "jti")
    refute Repo.get_by!(GuardianToken, jti: user_jti) == nil

    conn = conn
    |> post(user_path(conn, :change_profile, -1), [old_password: "abc", password: "wrong password", password_confirmation: "x"])

    assert get_flash(conn, :error) === "Unexpected user id."
  end

  test "does not allow changing the password if the old password is not correct", context do
    query = from p in User,
     select: [p.id, p.nickname, p.email]

    users = Repo.all(query)

    assert !Enum.empty?(users)

    conn =  build_conn()
    |> post("/auth/identity/callback", [email: context.user.email, password: "sekrit"])

    assert Guardian.Plug.current_resource(conn).id == context.user.id
    assert get_flash(conn, :info) == "Signed in as #{context.user.nickname}"

    conn = conn
    |> post(user_path(conn, :change_password, context.user.id), [old_password: "abc", password: "new_password", password_confirmation: "new_password"])

    assert get_flash(conn, :error) === "Password failure."
  end


  test "does not change the password if the password confirmation does not match", context do
    query = from p in User,
     select: [p.id, p.nickname, p.email]

    users = Repo.all(query)

    assert !Enum.empty?(users)

    conn =  build_conn()
    |> post("/auth/identity/callback", [email: context.user.email, password: "sekrit"])

    assert Guardian.Plug.current_resource(conn).id == context.user.id
    assert get_flash(conn, :info) == "Signed in as #{context.user.nickname}"

    conn = conn
    |> post(user_path(conn, :change_password, context.user.id), [old_password: "sekrit", password: "new_sekrit", password_confirmation: "x"])

    assert get_flash(conn, :error) === "New password and password confirmation does not match."
  end

  test "changes the password", context do
    query = from p in User,
     select: [p.id, p.nickname, p.email]

    users = Repo.all(query)

    assert !Enum.empty?(users)

    conn =  build_conn()
    |> post("/auth/identity/callback", [email: context.user.email, password: "sekrit"])

    assert Guardian.Plug.current_resource(conn).id == context.user.id
    assert get_flash(conn, :info) == "Signed in as #{context.user.nickname}"

    conn = conn
    |> post(user_path(conn, :change_password, context.user.id), [old_password: "sekrit", password: "new_sekrit", password_confirmation: "new_sekrit"])

    assert get_flash(conn, :info) === "Password changed."
    refute get_flash(conn, :error)

    case Repo.get_by(Authorization, user_id: to_string(context.user.id), provider: to_string(:identity)) do
      nil -> flunk "could not load changed password for validation."
      authorization ->
        assert Comeonin.Bcrypt.checkpw("new_sekrit", authorization.token)
    end
  end

  test "changes the profile information", context do
    query = from p in User,
     select: [p.id, p.nickname, p.email]

    users = Repo.all(query)

    assert !Enum.empty?(users)

    conn =  build_conn()
    |> post("/auth/identity/callback", [email: context.user.email, password: "sekrit"])

    assert Guardian.Plug.current_resource(conn).id == context.user.id
    assert get_flash(conn, :info) == "Signed in as #{context.user.nickname}"

    user = Repo.get_by!(User, id: to_string(context.user.id))

    conn = conn
    |> post(user_path(conn, :change_profile, context.user.id), [
      nickname: user.nickname <> "_UPDATED",
      last_name: user.last_name <> "_UPDATED",
      first_name: user.first_name <> "_UPDATED",
      phone: user.phone <> "_UPDATED"
    ])

    assert get_flash(conn, :info) === "Profile sucessfully updated."
    refute get_flash(conn, :error)

    case Repo.get_by(User, id: to_string(context.user.id)) do
      nil -> flunk "could not load changed password for validation."
      loaded_user ->
        assert user.nickname <> "_UPDATED"  === loaded_user.nickname
        assert user.last_name <> "_UPDATED" === loaded_user.last_name
        assert user.first_name <> "_UPDATED" === loaded_user.first_name
        assert user.phone <> "_UPDATED"     === loaded_user.phone
      end
  end


end
