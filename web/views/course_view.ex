defmodule Sportsgeist.CourseView do
  use Sportsgeist.Web, :view

  alias Sportsgeist.Course
  alias Sportsgeist.User

  @doc """
  Returns path to desired controller action and providing all required parameters
  as needed.
  """
  def action_for_mode(conn, _mode, _course \\ :nil)
  def action_for_mode(conn, :new, _course ), do: course_path(conn, :create)
  def action_for_mode(conn, :edit, %Ecto.Changeset{data: %Course{id: id}}), do: course_path(conn, :update, id)

  @doc """
  Join the list of the owner nicknames separated by ","
  """
  def owners(%Course{owner: owners}) do
   owners
   |> Enum.map(&(&1.nickname))
   |> Enum.join(", ")
 end

 @doc """
 Check if the given user is in the list of owners of the given course.
 """
 def is_course_owner?(%User{id: id}, %Course{owner: owners}) do
   owners
   |> Enum.any?(&(&1.id === id))
 end

 @doc """
 Returns a random course image
 """
 def course_image() do
   :rand.seed(:exsplus)
   ["pexels-photo-60230.jpeg","pexels-photo-87835.jpeg","sport-fitness-workout-resolution.jpg"]
   |> Enum.random
 end
end
