defmodule Sportsgeist.CourseTest do
  @moduledoc """
  Define a minimal user course for testing purposes
  """
  use Sportsgeist.ModelCase

  alias Sportsgeist.Course

  @valid_attrs %{name: "some content", iteration: "weekly", owner: [%{email: "jd@example.net", nickname: "John Doe"}]}
  @invalid_attrs %{x: "hallo"}

  test "changeset with valid attributes" do
    changeset = Course.changeset(%Course{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Course.changeset(%Course{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "fails on changeset without a owner" do
    changeset = Course.changeset(%Course{}, Map.delete(@valid_attrs, :owner))
    refute changeset.valid?
    assert changeset.errors === [owner: {"can't be blank", [validation: :required]}]
  end

  test "fails on changeset with empty name" do
    changeset = Course.changeset(%Course{}, Map.put(@valid_attrs, :name, ""))
    refute changeset.valid?
    assert changeset.errors === [name: {"can't be blank", [validation: :required]}]
  end

  test "fails on changeset when violating the names length constraint" do
    changeset = Course.changeset(%Course{}, Map.put(@valid_attrs, :name,generate_string(151)))
    refute changeset.valid?
    assert changeset.errors === [name: {"should be at most %{count} character(s)", [count: 150, validation: :length, max: 150]}]
  end

  defp generate_string(len) do
      ["a","b","c"]
      |> Stream.cycle
      |> Enum.take(len)
      |> Enum.join
  end
end
