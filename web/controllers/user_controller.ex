defmodule Sportsgeist.UserController do
  use Sportsgeist.Web, :controller

  # require authentification for all controller methods execept creating a new
  # user from the login page
  plug Guardian.Plug.EnsureAuthenticated, [handler: __MODULE__] when not action in [:new]

  require Logger

  alias Sportsgeist.Repo
  alias Sportsgeist.User
  alias Sportsgeist.UserController.PasswordChanger

  # handle the case where no authenticated user
  # was found
  def unauthenticated(conn, _params) do
    conn
    #|> put_status(401)
    |> put_flash(:error, "Authentication required")
    |> redirect(to: "/auth/identity")
  end


  @doc """
  Show from for creating a new user
  """
  def new(conn, _params, current_user, _claims) do
    render conn, "new.html", current_user: current_user
  end

  @doc """
  Shows current user information
  """
  def show(conn, %{"id" => id}, current_user, _claims) do
      user = Repo.get(User, id)
    render conn, "show.html", user: user, current_user: current_user
  end

  @doc """
  Change password information. Passwords of ':identity' authorizations could
  be changed. In order to change the password the user has to confirm the account
  by providing the current password. The new password must conform the password
  requirements and match the password confirmation. Futhermore the id of the
  currently logged in user and the user id of the user from the formular has to
  be the same.
  """
  def change_password(conn, params, current_user, _claims) do
    expected_id = current_user.id

    # all convert string parameter value of id to integer value
    id = get_user_id(params);

    Logger.debug "expected id [#{expected_id}] vs. given id [#{id}]"

    {msg_type, message} = case id do
      ^expected_id -> PasswordChanger.change_password(expected_id, params)
      _ -> {:error, gettext("Unexpected user id.")}
    end

    user = Repo.get(User, expected_id)

    conn
    |> put_flash(msg_type, message)
    |> render(:show, user: user, current_user: current_user)
  end

  @doc """
  Changes profile information (excluding passwords).
  """
  def change_profile(conn, params, current_user, _claims) do
    expected_id = current_user.id
    {msg_type, message} = case get_user_id(params) do
      ^expected_id ->

        changeset = User
        |> Repo.get(expected_id)
        |> User.changeset(params)

        case Repo.update(changeset) do
          {:ok, _user} ->
            Logger.debug("Profile data changed for  #{expected_id}.")
            {:info, gettext("Profile sucessfully updated.")}
          {:error, _changeset} ->
            {:error, gettext("Could not change username")}
        end

      id ->
        Logger.debug "expected id [#{expected_id}] vs. given id [#{id}]"
        {:error,gettext("Unexpected user id.")}
    end

    user = Repo.get(User, expected_id)

    conn
    |> put_flash(msg_type, message)
    |> render(:show, user: user, current_user: current_user)
  end

  # returns the user id from the request params as integer
  defp get_user_id(%{"id" => id} = _params) do
   {numerical_id, _decimal} = Integer.parse(id)
   numerical_id
  end
end
