defmodule Sportsgeist.UserController.PasswordChanger do
  @moduledoc """
  PasswordChanger implements password changing function for users. Changing the
  password is allowed if:
  * the new password and the password confirmation is the same
  * the old password matches the save auth token
  """

  import Sportsgeist.Gettext

  require Logger

  alias Sportsgeist.Repo
  alias Sportsgeist.Authorization

    @doc """
    Changes the password for the given user id. Returns a tuple with either an
    {':error', error_message} or {:info, success_message}
    """
    def change_password(id, params) do
      Logger.debug("Password change for user id #{id} requested.")

      Authorization
      |> Repo.get_by(user_id: id, provider: to_string(:identity))
      |> check_password(params)
      |> update_password(params)
    end

    # multiple definitions of a check_password method for different conditions
    # it needs a valid authorization (not 'nil')
    defp check_password(authorization, _password) when is_nil(authorization), do: {:error, gettext("Password not found.")}
    defp check_password(authorization, %{"old_password" => password}) do
      if Comeonin.Bcrypt.checkpw(password, authorization.token) do
        authorization
      else
        {:error, gettext("Password failure.")}
      end
    end

    # multipöe definitions for updating a password for different conditions.
    # (1) there is already an error message passed
    # (2) the passwords do not match
    # (3) everthing is fine :-)
    defp update_password({:error, message}, _params), do: {:error, message}
    defp update_password(_authorization, %{"password" => password, "password_confirmation" => password_confirmation}) when password !== password_confirmation, do: {:error, gettext("New password and password confirmation does not match.")}
    defp update_password(authorization,  %{"password" => password, "password_confirmation" => password_confirmation}) when password === password_confirmation do
      changeset = Authorization.changeset(authorization, %{token: Comeonin.Bcrypt.hashpwsalt(password)})
      case Repo.update(changeset) do
        {:ok, _authorization} ->
          Logger.debug("Password successfully changed for #{authorization.id}.")
          {:info, gettext("Password changed.")}
        {:error, _changeset} -> {:error, gettext("Could not change username")}
      end
    end
end
