# -:||----||:- Sportsgeist

# Credits
The authorization implementationn is taken from the [PhoenixGuardian](https://github.com/hassox/phoenix_guardian) project by [Hassox](https://github.com/hassox).

# Getting ready
To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `npm install`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix

# Infrastructure

* Development:
 * build status [![Build Status](https://semaphoreci.com/api/v1/0x0me/sportsgeist/branches/develop/badge.svg)](https://semaphoreci.com/0x0me/sportsgeist),
 * Code Coverage [![Coverage Status](https://coveralls.io/repos/bitbucket/0x0me/sportsgeist/badge.svg?branch=develop)](https://coveralls.io/bitbucket/0x0me/sportsgeist?branch=develop)

# Development
Start database container:

```
docker run --name sportsgeist-postgres -e POSTGRES_PASSWORD=<password> -p 5432:5432 -d postgres
```

or if already created

```
docker start sportsgeist-postgres
```

open a shell inside the container:

```
docker exec  -it "sportsgeist-postgres"  bash

#or run psql client immediatly
docker exec  -it "sportsgeist-postgres"  psql -U postgres
```

# Use docker-compose

```
# create new image(s)
docker-compose build

# run in foreground
docker-compose up
```

# dev notes

## Testing
### Run single test
run single test (eg.):
```
mix test test/controllers/course_controller_test.exs:67
```
### Debugging a single test
```
> MIX_ENV=test iex -S mix
```

in iex setup debugger
```
:debugger.start
:int.ni(Sportsgeist)
:int.ni(Sportsgeist.CourseController)
:int.ni(Sportsgeist.Course)
:int.break(Sportsgeist.CourseController, 114)
```

run the tests
```
Mix.Task.run "test"
```

```
select * from accounts where password_hash = crypt('mypassword', password_hash);
//(note how the existing hash is used as its own individualized salt)

Create a hash of :password with a great random salt:

insert into accounts (password) values crypt('mypassword', gen_salt('bf', 8));
//(the 8 is the work factor)
```

* apt install libpq-dev
* cd `pg_config --sharedir`
* echo "create extension pgcrypto" | psql -d sportsgeist_dev

```
 Ecto.Adapters.SQL.query(Sportsgeist.Repo, "select * from users where password = crypt($1, password)",["mypassword"])
 ```
