defmodule Sportsgeist.UserTest do
  @moduledoc """
  Define a minimal user model for testing purposes
  """
  use Sportsgeist.ModelCase

  alias Sportsgeist.User

  @valid_attrs %{nickname: "some content", email: "foo@example.com", is_admin: false}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "nickname has to be at least two characters long" do
    changeset = User.changeset(%User{}, %{nickname: "ni", email: "foo@example.com", is_admin: false})
    assert changeset.valid?

    changeset = User.changeset(%User{}, %{nickname: "n", email: "foo@example.com", is_admin: false})
    refute changeset.valid?
  end
end
