defmodule Sportsgeist.Router do
  use Sportsgeist.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    #plug Sportsgeist.Locale
  end

  # This plug will look for a Guardian token in the session in the default location
  # Then it will attempt to load the resource found in the JWT.
  # If it doesn't find a JWT in the default location it doesn't do anything
  pipeline :browser_auth do
    plug Guardian.Plug.VerifySession
    plug Guardian.Plug.LoadResource
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Sportsgeist do
    pipe_through [:browser, :browser_auth]  # Use the default browser stack

    get "/", PageController, :index
    delete "/logout", AuthController, :logout

    resources "/tokens", TokenController
    resources "/users", UserController
    post "/users/:id/password", UserController, :change_password
    post "/users/:id/profile", UserController, :change_profile
    resources "/courses", CourseController
  end

  # This scope is the main authentication area for Ueberauth
  scope "/auth", Sportsgeist do
    pipe_through [:browser, :browser_auth] # Use the default browser stack

    get "/:identity", AuthController, :login
    get "/:identity/callback", AuthController, :callback
    post "/:identity/callback", AuthController, :callback
  end

  # Other scopes may use custom stacks.
  # scope "/api", Sportsgeist do
  #   pipe_through :api
  # end
end
