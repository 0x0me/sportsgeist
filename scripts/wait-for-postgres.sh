#!/bin/bash

set -e

cmd="$@"

until psql -h "postgres" -U "postgres" -c '\l'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "cmd arg: ${cmd}"
>&2 echo "Postgres is up - entering app"
exec $cmd
