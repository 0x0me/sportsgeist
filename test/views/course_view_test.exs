defmodule Sportsgeist.CourseViewTest do
  use Sportsgeist.ConnCase, async: true

  alias Sportsgeist.CourseView
  alias Sportsgeist.Course
  alias Sportsgeist.User

  test "concat owners" do
    assert %Course{owner: [%User{id: 0, nickname: "Bart Simpson"}, %User{id: 1, nickname: "Homer Simpson"}]} |> CourseView.owners == "Bart Simpson, Homer Simpson"
  end

  test "user may own a course" do
    course = %Course{owner: [%User{id: 0, nickname: "Bart Simpson"}, %User{id: 1, nickname: "Homer Simpson"}]}
    current_user = %User{id: 1}

    assert CourseView.is_course_owner?(current_user, course) === true
  end

  test "a user may not own a course" do
    course = %Course{owner: [%User{id: 0, nickname: "Bart Simpson"}, %User{id: 1, nickname: "Homer Simpson"}]}
    current_user = %User{id: 2}

    assert CourseView.is_course_owner?(current_user, course) === false
  end

  test "path for new" do
    path = build_conn()
    |> CourseView.action_for_mode(:new)
    assert path === "/courses"
  end

  test "path for edit" do
    path = build_conn()
    |> CourseView.action_for_mode(:edit, %Ecto.Changeset{data: %Course{id: 1}})
    assert path === "/courses/1"
  end

end
