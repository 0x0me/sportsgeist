defmodule Sportsgeist.GuardianToken do
  @moduledoc """
  Issued JWT token.
  """
  use Sportsgeist.Web, :model

  alias Sportsgeist.Repo
  alias Sportsgeist.GuardianSerializer

  @primary_key {:jti, :string, []}
  @derive {Phoenix.Param, key: :jti}
  schema "guardian_tokens" do
    field :aud, :string
    field :iss, :string
    field :sub, :string
    field :exp, :integer
    field :jwt, :string
    field :claims, :map

    timestamps()
  end

  @doc """
  Find all tokens for a given user. Returns empty list if no token is found
  """
  def for_user(user) do
    case GuardianSerializer.for_token(user) do
      {:ok, aud} ->
        (from t in Sportsgeist.GuardianToken, where: t.sub == ^aud)
        |> Repo.all
      _ -> []
    end
  end
end
