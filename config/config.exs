# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

config :sportsgeist, Sportsgeist.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "sportsgeist_repo",
  username: "user",
  password: "pass",
  hostname: "localhost"


# General application configuration
config :sportsgeist,
  ecto_repos: [Sportsgeist.Repo]

# Configures the endpoint
config :sportsgeist, Sportsgeist.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "6vp02HX8H4gwiCCZQAd8K34B5Dcw8qb+nVozRn7XeElhEbhQmlrMV8J5sRU+SNfS",
  render_errors: [view: Sportsgeist.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Sportsgeist.PubSub,
           adapter: Phoenix.PubSub.PG2]

 # Configure default locale
 config :sportsgeist, Sportsgeist.Gettext, default_locale: "de"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure ueberauth providers
config :ueberauth, Ueberauth,
  providers: [
    identity: {Ueberauth.Strategy.Identity, [callback_methods: ["POST"]]},
    google: {Ueberauth.Strategy.Google, []}
  ]

config :guardian, Guardian,
  issuer: "Sportsgeist.#{Mix.env}",
  ttl: {30, :days},
  verify_issuer: true,
  serializer: Sportsgeist.GuardianSerializer,
  secret_key: to_string(Mix.env),
  hooks: GuardianDb,
  permissions: %{
    default: [
    :read_profile,
    :write_profile,
    :read_token,
    :revoke_token,
    ],
  }

  config :guardian_db, GuardianDb,
    repo: Sportsgeist.Repo,
    sweep_interval: 60 # 60 minutes

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
