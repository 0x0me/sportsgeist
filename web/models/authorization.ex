defmodule Sportsgeist.Authorization do
  @moduledoc """
  Models an authorization structure.
  """
  use Sportsgeist.Web, :model

  # note: the password is not saved to the database directly. Only in case
  # of the `identity` provider the password is uses as `token` and gets
  # persisted there
  schema "authorizations" do
    field :provider, :string
    field :uid, :string
    field :token, :string
    field :refresh_token, :string
    field :expires_at, :integer
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    belongs_to :user, Sportsgeist.User

    timestamps()
  end

  # define required fields for later reference
  @required_fields ~w(provider uid user_id token)a
  # define optional fields for later reference
  @optional_fields ~w(refresh_token expires_at)a

  @doc """
  # Validations
  * all required fields are existing
  * the user (`user_id`) exists in the user table aka known user
  * there is only one authoriation per provider
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> validate_required(@required_fields)
    |> foreign_key_constraint(:user_id)
    |> unique_constraint(:provider_uid)
  end
end
