  $(":input[name='last_name'], :input[name='first_name']").keyup(function(){
    updateNickname()
  })

  function toggleNickname(value) {
    console.log("Getting value ["+value+"]");
    $("input[type='text'][name='display_nickname']").prop("disabled",value);
  }

  function updateNickname() {
    //only update if the user will not provide its own nickname - check the
    // 'useNickname' state
    var useNickname = $("input[type='checkbox'][name='use_nickname']").is(':checked');
    if( typeof(useNickname) === 'undefined' || useNickname === true) {
      $(":input[name='nickname']").val($(":input[name='first_name']").val() + " " + $(":input[name='last_name']").val());
    }
    $(":input[name='display_nickname']").val($(":input[name='nickname']").val());
  }

  function updateRealNickname() {
    $(":input[name='nickname']").val($(":input[name='display_nickname']").val());
  }


$('input.awesomplete').on('awesomplete-selectcomplete', function(evt) {
  console.log("HALLO COMPLETE");
  /*  get user id and nickname from autocomplete whereas the id is the value
   *  attribute of the option element and the label is the text child inside
   *  the document.
   *
   *  from https://stackoverflow.com/questions/33995763/awesomplete-take-option-value
   *  suggest using the
   *
   *  var selectValue = evt.originalEvent.text.value;
   *  var selectLabel = evt.originalEvent.text.label;
   *
   *  instead of:
   *  var selectValue = this.value;
   *
   * TODO: put all the things into one element
   */
  var selectValue = evt.originalEvent.text.value;
  var selectLabel = evt.originalEvent.text.label;
  // $('ul.owners').append('<li style="margin-top: 1ex"><span class="label label-default foo">'+selectLabel+' <i class="glyphicon glyphicon-remove"></i></span></li>')
  //
  // var num = $('ul.owners').children('li').length;
  // var numP = num - 2;
  // var idx = num -1;
  // console.log('Number of children:'+num+'np:'+numP+', label ['+selectLabel+'], value ['+selectValue+']');
  //
  // $('#course_owner_'+numP+'_nickname').after('<input class="form-control" id="course_owner_'+idx+'_nickname" name="course[owner]['+idx+'][nickname]" type="text" value="'+selectLabel+'">');
  // $('#course_owner_'+numP+'_id').after('<input id="course_owner_'+idx+'_id" name="course[owner]['+idx+'][id]" type="hidden" value="'+selectValue+'">');

  insertOwner(selectValue, selectLabel);
  this.value = '';
});

function insertOwner(id, nickname) {
  var elem = $('ul#owners');
  var index = $(elem).children('li.owner').length;
  console.log("Inserting "+$(elem)+",index["+index+'],id['+id+'],nickname['+nickname+']');
  var selector = 'owner_'+id;
  var num = $(elem).children('li#owners-input').before(
    '<li style="margin-top: 1ex" class="owner '+selector+'">'
    + '<h4><span class="label label-default owner">'
    + nickname
    + ' <i class="glyphicon glyphicon-remove" onclick="removeOwner(\''+selector+'\')"></i>'
    + '</span></h4>'
    + '<span class="hidden">'
    + '<input class="form-control" id="course_owner_'+index+'_nickname" name="course[owner]['+index+'][nickname]" type="hidden" value="'+nickname+'">'
    + '<input id="course_owner_'+index+'_id" name="course[owner]['+index+'][id]" type="hidden" value="'+id+'">'
    + '</span>'
    + '</li>'
  );
}

function removeOwner(selector) {
  $('li.'+selector).remove();
}
