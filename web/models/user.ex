defmodule Sportsgeist.User do
  @moduledoc """
  Models a user in our application. The user has no own password persisted.
  It refers to the authorization token.
  """
  use Sportsgeist.Web, :model

  schema "users" do
    field :email, :string
    field :last_name, :string
    field :first_name, :string
    field :nickname, :string
    field :phone, :string
    field :is_phone_call_allowed, :boolean

    field :is_enabled, :boolean
    field :is_admin, :boolean

    has_many :authorizations, Sportsgeist.Authorization
    many_to_many :courses, Sportsgeist.Course, join_through: "owners"

    timestamps()
  end

  # define required field for later reference
  @required_fields ~w(email nickname)a
  # define optional fields for later reference
  @optional_fields ~w(last_name first_name phone is_phone_call_allowed is_enabled is_admin)a



  @doc """
  Create changeset base on the `model` and `params`.

  Invalid changeset is returned on empty params.
  """
  def changeset(model, params \\ :invalid) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> unique_constraint(:nickname)
    |> unique_constraint(:email)
    |> validate_required(@required_fields)
    |> validate_length(:nickname, min: 2, max: 50)
    |> validate_format(:email, ~r/@/)
  end
end
