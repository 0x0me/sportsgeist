defmodule Sportsgeist.Locale do
  @moduledoc """
  #Locale
  Small service to change locale.
  __For debugging purposes only - disabled by default!___
  """
  import Plug.Conn

  require Logger

  def init(_opts), do: nil

  # add 'plug Sportsgeist.Locale' to router.ex and call
  # http://localhost:4000/?locale=eo afterwards to change locale
  def call(conn, _opts) do
    Logger.debug( "Session locale [#{Gettext.get_locale(Sportsgeist.Gettext)}]" )
    case conn.params["locale"] || get_session(conn, :locale) do
      nil     -> conn
      locale  ->
        Gettext.put_locale(Sportsgeist.Gettext, locale)
        conn |> put_session(:locale, locale)
    end
  end
end
