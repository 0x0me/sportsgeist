defmodule Sportsgeist.CourseController do
  @moduledoc """
  Course interactions
  """
  use Sportsgeist.Web, :controller

  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  alias Sportsgeist.Course
  alias Sportsgeist.User

  require Logger

  # handle the case where no authenticated user
  # was found
  def unauthenticated(conn, _params) do
    conn
    |> put_status(401)
    |> put_flash(:error, "Authentication required")
    |> redirect(to: "/")
  end

  def index(conn, _params, current_user, _Claims) do
    courses = Course
    |> Repo.all
    |> Repo.preload(:owner)
    render conn, "index.html", course: courses, current_user: current_user
  end

  def new(conn, _params, current_user, _claims) do
    users = Repo.all(User)
    changeset = Course.changeset(%Course{owner: [current_user]})
    render conn, "new.html", changeset: changeset, users: users, current_user: current_user, mode: :new
  end



  def show(conn, %{"id" => id}, current_user, _claims) do
    course =
    Course
    |> Repo.get(id)
    |> Repo.preload(:owner)
    render conn, "show.html", course: course, current_user: current_user
  end


  def edit(conn, %{"id" => id}, current_user, _claims) do
    users = Repo.all(User)

    changeset = Course
    |> Repo.get(id)
    |> Repo.preload(:owner)
    |> Course.changeset

    render conn, "new.html", changeset: changeset, users: users, current_user: current_user, mode: :edit
  end

  #def update(conn, params, current_user, _claims) do
  #  IO.inspect params
  #  IO.inspect conn
  #end

  def update(conn, %{"course" => course_params}, current_user, _claims) do
    id = Map.get(course_params, "id")

    course = Course
    |> Repo.get(id)
    |> Repo.preload(:owner)

    tx = Repo.transaction(fn -> update_course(course, course_params) end)
    case tx do
      {:ok, course}  ->
        conn
        |> put_flash(:info, gettext("Course sucessfully updated."))
        |> render("show.html", course: course, current_user: current_user)
      {:error, changeset} ->
        users = Repo.all(User)

        conn
        |> render("new.html", changeset: changeset, users: users, current_user: current_user, mode: :edit)
    end
  end

  # Perform database transaction.
  defp update_course(course, course_params) do
    # map form entries for owners to Users to populate required owner field
    # if there are no owners passed from the form the method defaults to an
    # empty list
    user = get_owners(course_params)
    result = course
    |> Course.update_changeset(course_params)
    |> Ecto.Changeset.put_assoc(:owner, user)
    |> Repo.update

   case result do
     {:ok, course}  -> course
     {:error, reason} ->  Repo.rollback(reason)
   end
  end

  @doc """
  Create a new course entry. The course creation has to be transactional due to
  the m:n releationship (courses and users = owner).

  If the course creation succeeds the user is redirected to the index page of
  the course creation modul.

  On failure the changeset will be populated with the last user input submitted
  via form and the error messages from the validation of the changeset. These
  error messages are used to inform the user about necessary changes to produce
  a valid changeset.
  """
  def create(conn, %{"course" => course_params}, current_user, _claims) do
    tx = Repo.transaction(fn -> create_course(course_params) end)
    case tx do
      {:ok, course}  ->
        conn
        |> put_flash(:info, "#{course.name} created!")
        |> redirect(to: course_path(conn, :index))
      {:error, changeset} ->
        users = Repo.all(User)
        conn
        |> render("new.html", changeset: changeset, users: users, current_user: current_user, mode: :new)
    end
  end

  # Perform database transaction.
  defp create_course(course_params) do
    # map form entries for owners to Users to populate required owner field
    # if there are no owners passed from the form the method defaults to an
    # empty list
    user = get_owners(course_params)
    result = %Course{owner: user}
    |> Course.changeset(course_params)
    |> Ecto.Changeset.put_assoc(:owner, user)
    |> Repo.insert

   case result do
     {:ok, course}  -> course
     {:error, reason} ->  Repo.rollback(reason)
   end
  end

  # Resolve nicknames to a list of users
  defp get_owners(%{"owner" => owner}) do
     owner
     |> Map.values
     |> Enum.map(&Map.get(&1,"nickname"))
     |> Enum.map(&Repo.get_by(User, nickname: &1))
  end
  defp get_owners(_params), do: []

end
