defmodule Sportsgeist.PageController do
  use Sportsgeist.Web, :controller

  plug :put_layout, "frontpage.html"

  # handle the case where no authenticated user
  # was found
  def unauthenticated(conn, _params) do
    conn
    |> put_status(401)
    |> put_flash(:error, "Authentication required")
    |> redirect(to: "/")
  end

  def index(conn, _params, current_user, _claims) do
    render conn, "index.html", current_user: current_user
  end
end
