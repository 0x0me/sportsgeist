defmodule Sportsgeist.Owner do
  @moduledoc """
  Owners are technical supervisors or administrators for one or more courses.
  """
  use Sportsgeist.Web, :model

  @primary_key false
  schema "owners" do
    belongs_to :users, Sportsgeist.User
    belongs_to :courses, Sportsgeist.Course

    timestamps()
  end

  def changeset(model, params \\ :invalid) do
    model
    |> cast(params, [:user_id, :course_id])
    |> validate_required([:user_id, :course_id])
  end
end
