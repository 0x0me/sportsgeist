
use Mix.Config

# Configure your database
config :sportsgeist, Sportsgeist.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("DATABASE_POSTGRESQL_USERNAME"),
  password: System.get_env("DATABASE_POSTGRESQL_PASSWORD"),
  database: "sportsgeist_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
