defmodule Sportsgeist.Course do
  @moduledoc """
  # Course model
  * TODO: Decide wether the name of a course should be unique or not
  * TODO: add location information
  """
  use Sportsgeist.Web, :model

  schema "courses" do
    field :name, :string
    field :description, :string
    field :iteration, :string
    field :icon, :binary
    field :active, :boolean

    many_to_many :owner, Sportsgeist.User, join_through: "owners", on_replace: :delete

    timestamps()
  end

  @required_fields ~w(name iteration)a
  @optional_fields ~w(description icon active)a

  def changeset(model, params \\ :invalid) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    |> cast_assoc(:owner, required: true)
    |> unique_constraint(:name)
    |> validate_required(@required_fields)
    |> validate_length(:name, [min: 1, max: 150])
  end

  @doc """
  Updates a course.
  """
  def update_changeset(model, params) do
    model
    |> cast(params, @required_fields ++ @optional_fields)
    # disallows update of different number of owners
    #|> cast_assoc(:owner, required: true)
    |> unique_constraint(:name)
    |> validate_required(@required_fields)
    |> validate_length(:name, [min: 1, max: 150])
  end
end
